package com.store.redis;

import com.store.config.BeanConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.redis.core.RedisTemplate;

public class Publisher {

    public static void send(Object message) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(BeanConfig.class);
        RedisTemplate template = ctx.getBean(RedisTemplate.class);
        template.convertAndSend("store-backend-result", message);
    }
}
