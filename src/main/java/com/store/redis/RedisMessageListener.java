package com.store.redis;

import com.google.gson.Gson;
import com.store.entities.MessageObject;
import com.store.repository.RepositoryManager;
import com.store.rules.RuleManager;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import redis.clients.jedis.Jedis;


public class RedisMessageListener implements MessageListener {
//
//    @Autowired
//    RedisTemplate<String,Object> template;
    //@Autowired
//    MessagePublisher publisher;

    @Override
    public void onMessage(Message message, byte[] pattern) {
        //System.out.println("REDIS MESSAGE RECEIVED:"+message.toString());

        Gson gson=new Gson();
        MessageObject object=gson.fromJson(message.toString(),MessageObject.class);

        String user=object.getUser();
        String type=object.getType();
        String code=object.getCode();
        Object msg=object.getMessage();
        long timestamp=object.getTimestamp();

        System.out.println("RECEIVED MESSAGE USER: "+user+
                " TYPE:"+type+
                " CODE:"+code+
                " MESSAGE:"+msg+
                " TIMESTAMP:"+timestamp);

        if(type.equals("Event")){
            switch (code){
                case "CART_ADD":
                    try{

                        boolean result= RuleManager.canAddCart(timestamp);

                        Jedis publisher=new Jedis("localhost");

                        MessageObject response=new MessageObject(user,type,code,String.valueOf(result),System.currentTimeMillis()/1000);
                        publisher.publish("store-backend-result",gson.toJson(response));

                        if(result) {
                            RepositoryManager repositoryManager = new RepositoryManager();
                            repositoryManager.save(object);
                        }
                    }catch (Exception e){
                        System.out.println("Error saving message in repository");
                    }
            }
        }

    }
}
