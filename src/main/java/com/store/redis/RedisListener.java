package com.store.redis;

import com.google.gson.Gson;
import com.store.entities.MessageObject;
import com.store.repository.RepositoryManager;
import com.store.rules.RuleManager;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

public class RedisListener extends JedisPubSub{
    public void onMessage(String channel, String message) {
        Gson gson=new Gson();
        MessageObject object=gson.fromJson(message,MessageObject.class);

        String user=object.getUser();
        String type=object.getType();
        String code=object.getCode();
        Object msg=object.getMessage();
        long timestamp=object.getTimestamp();

        System.out.println("RECEIVED MESSAGE USER: "+user+
                " TYPE:"+type+
                " CODE:"+code+
                " MESSAGE:"+msg+
                " TIMESTAMP:"+timestamp);

        if(type.equals("Event")){
            switch (code){
                case "CART_ADD":
                    try{

                        boolean result= RuleManager.canAddCart(timestamp);

                        Jedis publisher=new Jedis("localhost");
                        publisher.publish("store-backend-result","TRUE");

                        if(result) {
                            RepositoryManager repositoryManager = new RepositoryManager();
                            repositoryManager.save(object);
                        }
                    }catch (Exception e){
                        System.out.println("Error saving message in repository");
                    }
            }
        }
    }

    public void onSubscribe(String channel, int subscribedChannels) {
        System.out.println("REDIS SUBSCRIBED");
    }

    public void onUnsubscribe(String channel, int subscribedChannels) {
    }

    public void onPSubscribe(String pattern, int subscribedChannels) {
    }

    public void onPUnsubscribe(String pattern, int subscribedChannels) {
    }

    public void onPMessage(String pattern, String channel,
                           String message) {
    }
}
