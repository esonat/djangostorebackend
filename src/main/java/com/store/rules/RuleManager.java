package com.store.rules;

import com.mongodb.*;

public class RuleManager {

    public static boolean canAddCart(long timestamp){
        boolean result=true;

         try{
            MongoClient mongoClient =new MongoClient("localhost",27017);

            DB db=mongoClient.getDB("storebackendDB");
            DBCollection coll=db.getCollection("messages");

             BasicDBObject query=new BasicDBObject();
             int interval=Integer.parseInt(System.getProperty("cart.add.interval"));

             query.put("timestamp",new BasicDBObject("$gt", timestamp-interval*1000));
             //query.put("timestamp",Long.valueOf("1481264678079"));
             DBCursor cursor=coll.find(query);

             int count=0;
             int cart_add_limit=Integer.parseInt(System.getProperty("cart.add.limit"));


             while(cursor.hasNext()){
                 count++;
                 System.out.println(cursor.next());
             }

             System.out.println("COUNT:"+count);

             if(count>=cart_add_limit){
                 System.out.println("CANNOT ADD ITEM TO CART");
                 return false;
             }

        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        return result;
    }
}
