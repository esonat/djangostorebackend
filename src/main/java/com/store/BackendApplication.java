package com.store;

import com.store.config.BeanConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;


@EnableScheduling
@ComponentScan("com.store")
@SpringBootApplication
@PropertySource(value="classpath:/application.properties")
@Import(BeanConfig.class)
public class BackendApplication {
    @Autowired
    Environment env;

//
//    @Bean
//    RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory, MessageListenerAdapter listenerAdapter){
//
//        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
//        container.setConnectionFactory(connectionFactory);
//        container.addMessageListener(listenerAdapter, new PatternTopic("chat"));
//
//        return container;
//    }

    public static void main(String[] args){
        SpringApplication.run(BackendApplication.class,args);

        System.setProperty("cart.add.interval","15");
        System.setProperty("cart.add.limit","5");

//        ApplicationContext ctx = new AnnotationConfigApplicationContext(BeanConfig.class);
//        RedisTemplate template = ctx.getBean(RedisTemplate.class);

//        Jedis jedis=new Jedis("localhost");
//        RedisListener listener = new RedisListener();
//        jedis.subscribe(listener, "store-backend");

//        try{
//            //String applicationId=System.getProperty("application.id");
//            //System.out.println(applicationId);
//            QueueConsumer consumer=new QueueConsumer("store-backend");
//
//            Thread consumerThread=new Thread(consumer);
//            consumerThread.start();
//        }catch (TimeoutException e){
//            e.printStackTrace();
//        }catch (IOException e){
//            e.printStackTrace();
//        }
    }
}
