package com.store.scheduled;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;

@Component
public class ScheduledTasks {
//
    @Autowired
    RedisTemplate<String,Object> template;

    private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

//    @Scheduled(fixedRate = 5000)
    public void sendMessage(){
//        ApplicationContext ctx = new AnnotationConfigApplicationContext(BeanConfig.class);
//        RedisTemplate template = ctx.getBean(RedisTemplate.class);
     //   template.convertAndSend("store-backend-result","MESSAGE");
    }
}
