package com.store.util;

public class TimeUtil {

    public static long getPastTimestamp(long interval){
        long unixTime = System.currentTimeMillis() / 1000L;
        return unixTime-interval;
    }
}
