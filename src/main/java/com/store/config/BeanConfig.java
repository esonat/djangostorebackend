package com.store.config;

import com.store.redis.RedisMessageListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import javax.annotation.Resource;

@Configuration
@PropertySource("classpath:/application.properties")
public class BeanConfig {
    @Resource
    Environment env;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer(){
        return new PropertySourcesPlaceholderConfigurer();
    }

//    @Bean
//    public JedisConnectionFactory jedisConnectionFactory(){
//        return new JedisConnectionFactory();
//    }
//
//
    @Bean
    public RedisConnectionFactory redisConnectionFactory(){
        JedisConnectionFactory connectionFactory=new JedisConnectionFactory();

        connectionFactory.setHostName("localhost");
        connectionFactory.setPort(6379);

        return connectionFactory;
    }


    @Bean
    public StringRedisSerializer stringRedisSerializer(){
        return new StringRedisSerializer();
    }


    @Bean
    public RedisTemplate<String,Object> redisTemplate(){
        RedisTemplate<String,Object> redisTemplate=new RedisTemplate<String,Object>();
        //redisTemplate.setConnectionFactory(jedisConnectionFactory());
        redisTemplate.setConnectionFactory(redisConnectionFactory());
//        redisTemplate.setStringSerializer(stringRedisSerializer());
//        redisTemplate.setDefaultSerializer(stringRedisSerializer());
        redisTemplate.setValueSerializer(stringRedisSerializer());
        
        return redisTemplate;
    }

    @Bean
    public MessageListenerAdapter messageListenerAdapter(){
        MessageListenerAdapter messageListenerAdapter=new MessageListenerAdapter(messageListener());
        messageListenerAdapter.setSerializer(stringRedisSerializer());
        return messageListenerAdapter;
    }
//
//    @Bean
//    public MessageListenerAdapter messageListener(RedisMessageListener listener){
//
//    }
//

    @Bean
    MessageListenerAdapter messageListener() {
        return new MessageListenerAdapter( new RedisMessageListener() );
    }

    @Bean
    RedisMessageListenerContainer redisContainer() {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();

        container.setConnectionFactory( redisConnectionFactory() );
        //container.setConnectionFactory( jedisConnectionFactory() );
        container.addMessageListener( messageListener(), new ChannelTopic( "store-backend" ) );

        return container;
    }

//    @Bean
//    MessagePublisher redisPublisher(){
//        return new RedisMessagePublisher(redisTemplate(),topic());
//    }

    @Bean
    ChannelTopic topic(){
        return new ChannelTopic("store-backend-result");
    }
}
