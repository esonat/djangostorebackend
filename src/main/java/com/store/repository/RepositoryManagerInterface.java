package com.store.repository;

import com.store.entities.MessageObject;

public interface RepositoryManagerInterface {
    void save(MessageObject message);
}
