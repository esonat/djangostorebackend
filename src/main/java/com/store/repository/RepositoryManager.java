package com.store.repository;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.store.entities.MessageObject;

public class RepositoryManager implements RepositoryManagerInterface {
    public void save(MessageObject message){
        try{
            MongoClient mongoClient =new MongoClient("localhost",27017);

            DB db=mongoClient.getDB("storebackendDB");
            DBCollection coll=db.getCollection("messages");
            System.out.println("Collection messages selected successfully");

            BasicDBObject doc=new BasicDBObject()
                    .append("user",message.getUser())
                    .append("type",message.getType())
                    .append("code",message.getCode())
                    .append("message",message.getMessage())
                    .append("timestamp",message.getTimestamp());

            coll.insert(doc);
            System.out.println("Document inserted successfully");

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
