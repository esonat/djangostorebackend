package com.store.messaging.entities;

import java.io.Serializable;

public class MessageObject implements Serializable{
    String user;
    String type;
    Object message;

    public MessageObject(){}

    public MessageObject(String user,String type,Object message){
        this.user=user;
        this.type=type;
        this.message=message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
