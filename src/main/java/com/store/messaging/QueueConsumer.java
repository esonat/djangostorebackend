package com.store.messaging;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;
import com.store.messaging.entities.MessageObject;
import org.apache.commons.lang3.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class QueueConsumer extends Endpoint implements Runnable,Consumer,QueueConsumerInterface {
    Logger logger= LoggerFactory.getLogger(QueueConsumer.class);
    private static String LNAME="QUEUECONSUMER";

    public QueueConsumer(String endpointName) throws IOException,TimeoutException{
        super(endpointName);
    }

    public void run(){
        try{
            channel.basicConsume(endpointName,true,this);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public void handleConsumeOk(String consumerTag){
        System.out.println("Consumer "+consumerTag+" registered");
    }

    public void handleDelivery(String consumerTag, Envelope env, BasicProperties props,byte[] body) throws IOException{
        String MNAME="HANDLE DELIVERY";
        try {
            MessageObject message = (MessageObject) SerializationUtils.deserialize(body);

            switch (message.getType()){
                case "Event":
                    System.out.println("RECEIVED EVENT MESSAGE"+message.getMessage());
                    break;
            }

        }catch (Exception e){
            System.out.println("Exception while deserializing message");
        }
    }

    public void handleCancel(String consumerTag) {}
    public void handleCancelOk(String consumerTag) {}
    public void handleRecoverOk(String consumerTag) {}
    public void handleShutdownSignal(String consumerTag, ShutdownSignalException arg1) {}
}
