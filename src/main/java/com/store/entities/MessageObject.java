package com.store.entities;


import java.io.Serializable;

public class MessageObject implements Serializable{
    String user;
    String type;
    String code;
    Object message;
    long timestamp;

    public MessageObject(){}

    public MessageObject(String user,String type,String code,Object message,long timestamp){
        this.user=user;
        this.type=type;
        this.code=code;
        this.message=message;
        this.timestamp=timestamp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
